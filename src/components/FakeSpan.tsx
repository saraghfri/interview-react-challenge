import {useEffect, useRef} from "react";

interface FakeSpanProps {
    textValue: string;
    setFakeSpanDomWidth: (width: string) => void;
}

function FakeSpan({ textValue, setFakeSpanDomWidth }: FakeSpanProps) {
    const ref = useRef<HTMLSpanElement>(null);

    useEffect(() => {
        if (ref.current) {
            const newWidth = getComputedStyle(ref.current).width;
            setFakeSpanDomWidth(newWidth);
        }
    }, [textValue]);

    return (
        <span ref={ref} className="invisible absolute whitespace-pre">
      {textValue}
    </span>
    );
}

export default FakeSpan;