import {useEffect, useRef, useState} from "react";
// @ts-ignore
import FakeSpan from './FakeSpan.tsx'

interface ServiceSearchResult {
    name: string;
}

interface FeaturefulSearchInputProps {
    serviceSearchInputValue: string;
    setServiceSearchInputValue: (value: string) => void;
    serviceSearchResults: ServiceSearchResult[];
}

function FeaturefulSearchInput({
                                   serviceSearchInputValue,
                                   setServiceSearchInputValue,
                                   serviceSearchResults,
                               }: FeaturefulSearchInputProps) {
    const [fakeSpanDomWidth, setFakeSpanDomWidth] = useState<string>("100%");
    const [suggestionInputPlaceholder, setSuggestionInputPlaceholder] =
        useState<string>("");

    const inputRef = useRef<HTMLInputElement>(null);

    const handleSearchInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;
        setServiceSearchInputValue(value);
    };

    const firstSearchItemText = serviceSearchResults?.[0]?.name || "";

    const isInputValueSubstrinOfFirstSearchItem =
        serviceSearchInputValue === firstSearchItemText.substring(0, serviceSearchInputValue.length);

    useEffect(() => {
        const newPlaceholder = isInputValueSubstrinOfFirstSearchItem ? firstSearchItemText : "";
        setSuggestionInputPlaceholder(newPlaceholder);
    }, [serviceSearchInputValue, serviceSearchResults]);

    const shouldInputExpand = fakeSpanDomWidth === "0px" || suggestionInputPlaceholder.length === 0;

    return (
        <div className="flex w-full">
            <div className="flex justify-between items-center w-full relative overflow-hidden">
                <input
                    ref={inputRef}
                    className="absolute outline-none text-gray-700"
                    style={{width: shouldInputExpand ? "100%" : fakeSpanDomWidth}}
                    value={serviceSearchInputValue}
                    onChange={handleSearchInputChange}
                    placeholder="به چه خدمتی نیاز دارید؟"
                />

                <FakeSpan
                    textValue={serviceSearchInputValue}
                    setFakeSpanDomWidth={setFakeSpanDomWidth}
                />

                <input
                    placeholder={suggestionInputPlaceholder}
                    className="w-full outline-none"
                />
            </div>
        </div>
    );
}

export default FeaturefulSearchInput;