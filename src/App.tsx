import {useEffect, useState} from "react";
// @ts-ignore
import FeaturefulSearchInput from './components/FeaturefulSearchInput.tsx'
// @ts-ignore
import SearchResults from './components/SearchResults.tsx'

interface Service {
    name: string;
}

function App() {
    const [serviceSearchInputValue, setServiceSearchInputValue] = useState<string>("");
    const [services, setServices] = useState<Service[]>([]);
    const [filteredServices, setFilteredServices] = useState<Service[]>([]);

    useEffect(() => {
        fetch("/api/services?zoneId=1")
            .then((res) => res.json())
            .then((s: Service[]) => setServices(s));
    }, []);

    useEffect(() => {
        const results = services.filter((s) => s.name.includes(serviceSearchInputValue));
        setFilteredServices(serviceSearchInputValue.trim() !== "" ? results : []);
    }, [serviceSearchInputValue, services]);

    return (
        <div className="fixed top-0 bottom-0 left-0 right-0 bg-white z-[99999] cursor-default">
            <div className="flex flex-col items-center justify-center w-full h-full px-3 max-w-96 m-auto">
                <h1 className='mb-3'>خدمت مورد نیاز شما</h1>
                <div className="relative flex items-center w-full shadow-sm rounded-md">
                    <div className="flex flex-col justify-center w-full relative">
                        <div className="flex items-center p-3 border-gray-200 border rounded-lg h-12">
                            <FeaturefulSearchInput
                                serviceSearchInputValue={serviceSearchInputValue}
                                setServiceSearchInputValue={setServiceSearchInputValue}
                                serviceSearchResults={filteredServices}
                            />
                        </div>
                        <span className="absolute left-0 self-center h-9 w-0 border-r-[1px] border-gray-200"/>
                    </div>
                    <SearchResults
                        containerClassName="bg-white overflow-auto z-10 mt-12 top-0 max-h-72 absolute w-full border border-[#EAECED] rounded-b-md shadow-sm scrollbar-minimal"
                        results={filteredServices}
                    />
                </div>
            </div>
        </div>
    );
}

export default App;